import concurrent.futures
import datetime
import json

from hashlib import md5
from pathlib import Path
from typing import Dict, Iterable, Union
from urllib.parse import urlparse

from mycroft.messagebus import Message
from mycroft.skills.common_play_skill import CPSMatchLevel, CommonPlaySkill
from mycroft.util.log import LOG
from mycroft.util.parse import match_one

import kodijson
import requests

from requests.exceptions import RequestException


class MediaNotFoundError(Exception):
    pass


class Kodi:
    """
    A thin wrapper around kodijson that handles tv show lookups in a friendlier
    interface.
    """

    # Kodi's search API isn't all that smart, and won't, for example, find
    # "Star Trek Deep Space 9" when you search for "Deep Space Nine".  Mycroft
    # however has a handy `match_one()` function that does a fuzzy match
    # against a list, so we just ask Kodi for everything and apply this
    # function to the results.
    SEARCH_EVERYTHING_FILTER = {
        "field": "title",
        "operator": "startswith",
        "value": "",
    }

    def __init__(self, endpoint: str):
        self.kodi = self.get_client(endpoint)
        self.logger = LOG.create_logger(self.__class__.__name__)

    @staticmethod
    def get_client(endpoint: str) -> kodijson.Kodi:
        """
        The kodi-json library doesn't understand standard BasicAuth URLs,
        instead requiring you to extract the username & password from the URL
        and pass them as separate arguments.

        Note:
          This is just copy/pasted directly from the majel library.  If it
          turns out that we need to pull more stuff from there, we may want to
          refactor things and make majel a dependency for this skill.
        """

        p = urlparse(endpoint)

        url = f"{p.scheme}://{p.hostname}{p.path}"
        if p.port:
            url = f"{p.scheme}://{p.hostname}:{p.port}{p.path}"

        kwargs = {}
        if p.username:
            kwargs["username"] = p.username
        if p.password:
            kwargs["password"] = p.password

        return kodijson.Kodi(url, **kwargs)

    def search(self, title: str) -> Dict[str, int]:

        try:
            return self._find_closest_match(title, "movies")
        except RequestException as e:
            raise MediaNotFoundError(
                f"Connectivity to Kodi appears to be broken: {e}"
            )
        except MediaNotFoundError:
            self.logger.info("No movies found in Kodi")

        show_id = self._find_closest_match(title, "tvshows")

        self.logger.info("Found show id: %s", show_id)

        return self._find_episode(show_id["tvshowid"])

    def _find_closest_match(self, title, key) -> Dict[str, int]:

        fn_map = {
            "movies": self.kodi.VideoLibrary.GetMovies,
            "tvshows": self.kodi.VideoLibrary.GetTVShows,
        }

        self.logger.info("Querying Kodi")
        listing = fn_map[key](filter=self.SEARCH_EVERYTHING_FILTER)[
            "result"
        ].get(key)

        match, confidence = match_one(title, {_["label"]: _ for _ in listing})

        if confidence > 0.7:

            self.logger.info(
                "Record found with %s confidence: %s",
                confidence,
                match["label"],
            )

            if key == "movies":
                return {"movieid": match["movieid"]}

            return {"tvshowid": match["tvshowid"]}

        raise MediaNotFoundError(
            "Nothing in the Kodi db matches the request: %s:%s", key, title
        )

    def _find_episode(self, show_id: int) -> Dict[str, int]:
        """
        We do the episode lookup here (as opposed to doing it in the monitor
        script) because I'd like to be able to use voice commands to specify
        season and episode later.
        """

        r = self.kodi.VideoLibrary.GetEpisodes(
            tvshowid=show_id,
            properties=["season", "episode", "playcount"],
            filter={
                "field": "playcount",
                "operator": "lessthan",
                "value": "1",
            },
        )

        episodes = r["result"].get("episodes")

        if not episodes:
            raise MediaNotFoundError("No episodes found in Kodi")

        for ep in episodes:
            if ep["playcount"] == 0:
                return {"episodeid": ep["episodeid"]}

        return {"episodeid": episodes[0]["episodeid"]}


class StreamingServices:

    CACHE = Path("/root/cache")
    UTELLY_HOST = "utelly-tv-shows-and-movies-availability-v1.p.rapidapi.com"
    UTELLY_ENDPOINT = f"https://{UTELLY_HOST}/lookup"

    def __init__(
        self,
        utelly_api_key: str,
        utelly_country: str,
        streaming_service_order: str,
    ):
        self.utelly_api_key = utelly_api_key
        self.utelly_country = utelly_country
        self.streaming_service_order = streaming_service_order.split(",")
        self.logger = LOG.create_logger(self.__class__.__name__)

    def search(self, query: str) -> str:
        return self._get_location_by_availability(self._get_locations(query))

    def _get_locations(self, query):

        cache_file = self.CACHE / md5(query.encode()).hexdigest()
        if cache_file.exists():
            with open(cache_file) as f:
                self.logger.info(f'Cache hit for "{query}"')
                return json.load(f)

        response = requests.get(
            self.UTELLY_ENDPOINT,
            headers={
                "x-rapidapi-host": self.UTELLY_HOST,
                "x-rapidapi-key": self.utelly_api_key,
            },
            params={"term": query, "country": self.utelly_country},
        )
        try:
            locations = response.json()["results"][0]["locations"]
        except (KeyError, IndexError):
            raise MediaNotFoundError()

        with open(cache_file, "w") as f:
            f.write(json.dumps(locations))

        return locations

    def _get_location_by_availability(
        self, locations: Iterable
    ) -> Union[str, None]:
        """
        Given a list of locations from the API, look through it to see if those
        locations match any of our supported video services and return the
        first one that matches.
        """

        for location in locations:

            if "netflix" in self.streaming_service_order:
                if location["display_name"] == "Netflix":
                    return location["url"]

            if "amazon" in self.streaming_service_order:
                if location["display_name"] == "Amazon Prime Video":
                    return location["url"]
                if location["display_name"] == "Amazon Instant Video":
                    return location["url"]

        raise MediaNotFoundError(
            "None of your selected video services are able to play that "
            "request."
        )


class StreamerSkill(CommonPlaySkill):
    """
    Messages emitted:
      * skill.majel.browser.open
      * skill.majel.browser.stop
      * skill.majel.kodi.play
      * skill.majel.kodi.stop
    """

    STOP_THRESHOLD = 10  # Seconds

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)

        self.kodi = None
        self.streamers = None
        self.started_at = None

        kodi_endpoint = self.settings.get("kodi_endpoint")
        utelly_api_key = self.settings.get("utelly_api_key")
        utelly_country = self.settings.get("utelly_country")
        streaming_service_order = self.settings.get("streaming_service_order")

        if kodi_endpoint:
            self.kodi = Kodi(endpoint=kodi_endpoint)
        else:
            self.log.warning(
                "Kodi hasn't been configured with an endpoint.  If you're "
                "planning on communicating with a local Kodi install, you'll "
                "need to log into https://home.mycroft.ai/ , go to "
                '"Skills" » "Streamer" » "Local Streaming Services" and enter'
                "it there."
            )

        if utelly_api_key and utelly_country and streaming_service_order:
            self.streamers = StreamingServices(
                utelly_api_key=utelly_api_key,
                utelly_country=utelly_country,
                streaming_service_order=streaming_service_order,
            )
        else:
            self.log.warning(
                "Your streaming services haven't been configured, so you "
                "can't play anything with them yet.  To set them up, you'll "
                "need to log into https://home.mycroft.ai/ and go to "
                '"Skills" » "Streamer" and enter the information there.  The '
                "UTelly API key can be acquired by going to "
                "https://rapidapi.com/utelly/api/utelly"
            )

    def stop(self):
        """
        Whenever a play event is issued, Mycroft sends a stop event right
        afterward and I have no idea why.  But the current behaviour is such
        that `CPS_start()` is run and almost immediately after `stop()` is
        called.  Why stop isn't called *first* and play second is beyond me.
        """

        if self.started_at:
            now = datetime.datetime.utcnow()
            if (now - self.started_at).seconds < self.STOP_THRESHOLD:
                self.log.info(
                    "Ignoring stop event as it's too soon after play has "
                    "started."
                )
                return

        self.started_at = None

        self.bus.emit(Message("skill.majel.kodi.stop"))
        self.bus.emit(Message("skill.majel.browser.stop"))

    # Video finders

    def try_kodi(self, phrase: str) -> Union[dict, None]:

        if not self.kodi:
            self.log.info("Skipping Kodi as it's not configured.")
            raise MediaNotFoundError("Unavailable")

        self.log.info("Trying Kodi")
        try:
            return dict(player="media", **self.kodi.search(phrase))
        except MediaNotFoundError as e:
            self.log.info(e)
            raise

    def try_streamers(self, phrase: str) -> Union[dict, None]:

        if not self.streamers:
            self.log.info("Skipping streamers as they're not configured.")
            raise MediaNotFoundError("Unavailable")

        self.log.info("Trying streamers")
        try:
            return {"player": "browser", "url": self.streamers.search(phrase)}
        except MediaNotFoundError as e:
            self.log.info(e)
            raise

    # Mycroft's Common Play Framework

    def CPS_match_query_phrase(self, phrase: str) -> Union[tuple, None]:
        with concurrent.futures.ThreadPoolExecutor(max_workers=3) as executor:
            futures = (
                executor.submit(self.try_kodi, phrase),
                executor.submit(self.try_streamers, phrase),
            )
            concurrent.futures.wait(futures)
            for future in futures:
                try:
                    return phrase, CPSMatchLevel.MULTI_KEY, future.result()
                except MediaNotFoundError:
                    continue

    def CPS_start(self, phrase, data: dict) -> None:

        self.started_at = datetime.datetime.utcnow()

        self.log.info("CBP start received with %s", data)

        player = data.pop("player")

        if player == "media":
            self.bus.emit(
                Message(
                    "skill.majel.kodi.play",
                    data,
                )
            )
        elif player == "browser":
            self.bus.emit(
                Message(
                    "skill.majel.browser.open",
                    {"url": data["url"]},
                )
            )


def create_skill():
    return StreamerSkill()
