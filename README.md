# streamer-skill

A Mycroft skill that leverages [Majel](https://pypi.org/project/majel/) to
search for & play from various video streaming services.


## Configuration

This skill leverages Mycroft's [Common Play Framework](https://mycroft-ai.gitbook.io/docs/skill-development/skill-types/common-play-framework)
to query a variety of sources for available options in playing a requested
video.  To do this, Mycroft must have three pieces of information, all of which
can be set by logging into [home.mycroft.ai](https://home.mycroft.ai/) and
editing the settings for this skill there:


### Streaming Services

Which streaming services do you want to search when you say
`play [some tv show]`?  Options are currently `kodi`, `netflix`, and `amazon`.
Separate multiple options with a comma and no spaces.

Default: `kodi,netflix,amazon`


### UTelly API Key

We use a (free) third-party service called [Utelly](https://www.utelly.com/) to
find out which streaming services have access to which media choices.  This is
how we know to point you to Netflix when you say "Altered Carbon" and Amazon
when you say "The Expanse".

While this service is free, you still have to sign up for it and acquire an API
key.  That key then needs to be entered into the settings for this skill.  Just
go to [rapid.api.com](https://rapidapi.com/utelly/api/utelly), register for a
free account, and get your API key for Utelly.

Note that requests in excess of 1000/month will cost you, but as this skill
caches the results, it's extremely unlikely that you'll ever breach 100.

Default: `""`


### Local Streaming Services (Kodi)

If you have a local Kodi installation, you can use its database to remember
what you were watching last and even how much of a file you've watched so far.
Its search tools can also be used so that if you say "Play the Expanse" and you
have local copies of the episodes, Mycroft will play your local copies instead
of the ones on Amazon (this is determined by the priority order in
`Streaming Services` above.)

In order to leverage the Kodi database like this, we need to know the JSON-RPC
endpoint for your server on your network.  It's likely something like
`http://192.168.0.123:8080/jsonrpc`

Default: `""`


## Use

Try one of the following:

> *"Hey Mycroft, play Altered Carbon"* - This should open Netflix and start
> playing wherever you left off.

> *"Hey Mycroft, play The Expanse"* - This should open Amazon Prime and start
> playing wherever you left off.
